<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['title', 'video_url', 'gif_url', 'user_id'];

    public function user(){
    	
    	return $this->belongsTo(User::class);

    }

    public function getFullVideoUrlAttribute(){

    	if($this->video_url){
    		return config('app.s3_url') . '/' . $this->video_url;
    	}
    	
    }

    public function getFullGifUrlAttribute(){

    	if($this->gif_url){
    		return config('app.s3_url') . '/' . $this->gif_url;
    	}

    }
}
